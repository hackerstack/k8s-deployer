FROM alpine:latest
RUN apk add --no-cache curl gettext
RUN addgroup -S gke-deployer && adduser -S -G gke-deployer gke-deployer
ENV KUBECTL_VERSION=v1.25.3
RUN curl -sLO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl
USER gke-deployer
